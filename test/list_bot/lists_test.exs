defmodule ListBot.ListsTest do
  use ListBot.DataCase

  alias ListBot.Lists

  describe "list_items" do
    alias ListBot.Lists.ListItem

    @valid_attrs %{done_at: "2010-04-17T14:00:00.000000Z", value: "some value"}
    @update_attrs %{done_at: "2011-05-18T15:01:01.000000Z", value: "some updated value"}
    @invalid_attrs %{done_at: nil, value: nil}

    def list_item_fixture(attrs \\ %{}) do
      {:ok, list_item} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Lists.create_list_item()

      list_item
    end

    test "list_list_items/0 returns all list_items" do
      list_item = list_item_fixture()
      assert Lists.list_list_items() == [list_item]
    end

    test "get_list_item!/1 returns the list_item with given id" do
      list_item = list_item_fixture()
      assert Lists.get_list_item!(list_item.id) == list_item
    end

    test "create_list_item/1 with valid data creates a list_item" do
      assert {:ok, %ListItem{} = list_item} = Lists.create_list_item(@valid_attrs)
      assert list_item.done_at == DateTime.from_naive!(~N[2010-04-17T14:00:00.000000Z], "Etc/UTC")
      assert list_item.value == "some value"
    end

    test "create_list_item/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Lists.create_list_item(@invalid_attrs)
    end

    test "update_list_item/2 with valid data updates the list_item" do
      list_item = list_item_fixture()
      assert {:ok, %ListItem{} = list_item} = Lists.update_list_item(list_item, @update_attrs)
      assert list_item.done_at == DateTime.from_naive!(~N[2011-05-18T15:01:01.000000Z], "Etc/UTC")
      assert list_item.value == "some updated value"
    end

    test "update_list_item/2 with invalid data returns error changeset" do
      list_item = list_item_fixture()
      assert {:error, %Ecto.Changeset{}} = Lists.update_list_item(list_item, @invalid_attrs)
      assert list_item == Lists.get_list_item!(list_item.id)
    end

    test "delete_list_item/1 deletes the list_item" do
      list_item = list_item_fixture()
      assert {:ok, %ListItem{}} = Lists.delete_list_item(list_item)
      assert_raise Ecto.NoResultsError, fn -> Lists.get_list_item!(list_item.id) end
    end

    test "change_list_item/1 returns a list_item changeset" do
      list_item = list_item_fixture()
      assert %Ecto.Changeset{} = Lists.change_list_item(list_item)
    end
  end
end
