# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :list_bot,
  ecto_repos: [ListBot.Repo]

# Configures the endpoint
config :list_bot, ListBotWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "iLhGQnMCulLKbDA7soI4Plp4sYXMgqEt0uul9oOz1hpcRdnN5V1Qbb+cLyiU8Fpe",
  render_errors: [view: ListBotWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: ListBot.PubSub,
  live_view: [signing_salt: "TLOsRQW6"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
