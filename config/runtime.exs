import Config

config :list_bot, ListBot.TelegramApi, api_key: System.get_env("TELEGRAM_API_KEY")

if System.get_env("BOT_APP_ENV") == "prod" do
  database_url =
    System.get_env("DATABASE_URL") ||
      raise """
      environment variable DATABASE_URL is missing.
      For example: ecto://USER:PASS@HOST/DATABASE
      """

  config :list_bot, ListBot.Repo,
    # ssl: true,
    database: database_url,
    pool_size: String.to_integer(System.get_env("POOL_SIZE") || "5")

  secret_key_base =
    System.get_env("SECRET_KEY_BASE") ||
      raise """
      environment variable SECRET_KEY_BASE is missing.
      You can generate one by calling: mix phx.gen.secret
      """

  config :list_bot, ListBotWeb.Endpoint,
    http: [
      port: String.to_integer(System.get_env("PORT") || "4000"),
      transport_options: [socket_opts: [:inet6]]
    ],
    secret_key_base: secret_key_base

  config :list_bot, ListBotWeb.Endpoint, server: true
end
