defmodule App do
  def restart do
    Application.stop(:list_bot)
    recompile()
    Application.ensure_all_started(:list_bot)
  end
end
