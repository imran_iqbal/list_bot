defmodule ListBot.Lists do
  @moduledoc """
  The Lists context.
  """

  import Ecto.Query, warn: false
  alias ListBot.Repo

  alias ListBot.Lists.ListItem

  @doc """
  Returns the list of list_items.

  ## Examples

      iex> list_list_items()
      [%ListItem{}, ...]

  """
  def list_list_items do
    Repo.all(ListItem)
  end

  def get_pending_list_items(user_id) do
    Repo.all(
      from li in "list_items",
        where: li.user_id == ^user_id,
        where: is_nil(li.done_at),
        order_by: [asc: :inserted_at],
        select: {li.id, li.value}
    )
  end

  def mark_item_as_done(user_id, item_id) do
    now = DateTime.utc_now()

    from(li in "list_items",
      where: li.id == ^item_id,
      where: li.user_id == ^user_id,
      update: [set: [done_at: ^now, updated_at: ^now]]
    )
    |> Repo.update_all([])
  end

  def enumerate_pending_items(user_id) do
    get_pending_list_items(user_id)
    |> Enum.with_index()
  end

  @doc """
  Gets a single list_item.

  Raises `Ecto.NoResultsError` if the List item does not exist.

  ## Examples

      iex> get_list_item!(123)
      %ListItem{}

      iex> get_list_item!(456)
      ** (Ecto.NoResultsError)

  """
  def get_list_item!(id), do: Repo.get!(ListItem, id)

  @doc """
  Creates a list_item.

  ## Examples

      iex> create_list_item(%{field: value})
      {:ok, %ListItem{}}

      iex> create_list_item(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_list_item(attrs \\ %{}) do
    %ListItem{}
    |> ListItem.create_changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a list_item.

  ## Examples

      iex> update_list_item(list_item, %{field: new_value})
      {:ok, %ListItem{}}

      iex> update_list_item(list_item, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_list_item(%ListItem{} = list_item, attrs) do
    list_item
    |> ListItem.update_changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a list_item.

  ## Examples

      iex> delete_list_item(list_item)
      {:ok, %ListItem{}}

      iex> delete_list_item(list_item)
      {:error, %Ecto.Changeset{}}

  """
  def delete_list_item(%ListItem{} = list_item) do
    Repo.delete(list_item)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking list_item changes.

  ## Examples

      iex> change_list_item(list_item)
      %Ecto.Changeset{data: %ListItem{}}

  """
  def change_list_item(%ListItem{} = list_item, attrs \\ %{}) do
    ListItem.changeset(list_item, attrs)
  end
end
