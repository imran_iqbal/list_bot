defmodule ListBot.Lists.ListItem do
  use ListBot.Db.Schema
  import Ecto.Changeset

  schema "list_items" do
    field :done_at, :utc_datetime_usec
    field :value, :string
    field :user_id, :id

    timestamps()
  end

  @doc false
  def create_changeset(list_item, attrs) do
    list_item
    |> cast(attrs, [:value, :user_id])
    |> validate_required([:value, :user_id])
    |> foreign_key_constraint(:user_id)
  end

  def update_changeset(list_item, attrs) do
    list_item
    |> cast(attrs, [:done_at])
    |> validate_required([:done_at])
  end
end
