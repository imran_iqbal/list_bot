defmodule ListBot.Accounts.User do
  use ListBot.Db.Schema
  import Ecto.Changeset

  schema "users" do
    field :first_name, :string
    field :is_admin, :boolean, default: false
    field :last_name, :string
    field :telegram_id, :integer

    timestamps()
  end

  @doc false
  def changeset(user, attrs) do
    user
    |> cast(attrs, [:first_name, :last_name, :telegram_id, :is_admin])
    |> validate_required([:first_name, :last_name, :telegram_id, :is_admin])
    |> unique_constraint(:telegram_id)
  end
end
