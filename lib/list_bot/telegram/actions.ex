defmodule ListBot.Telegram.Actions do
  @bad_command {:bad_command, ""}

  def get_command_payload(text, entities) do
    case Enum.find(entities, &match?(%{"type" => "bot_command"}, &1)) do
      nil ->
        @bad_command

      %{"length" => length, "offset" => offset} ->
        create_payload(text, length, offset)
    end
  end

  def do_action({user, {:done, display_id}}) do
    case Integer.parse(display_id) do
      :error -> "Not a valid item id"
      {index, ""} -> do_done_action(user.id, index - 1)
      _ -> "Not a valid item id"
    end
  end

  def do_action({_user, {:add, ""}}) do
    "Can not add empty items"
  end

  def do_action({user, {:add, text}}) do
    {:ok, _item} = ListBot.Lists.create_list_item(%{value: text, user_id: user.id})
    "Added"
  end

  def do_action({user, {:list, _}}) do
    response =
      ListBot.Lists.enumerate_pending_items(user.id)
      |> Enum.map(&format_list_item/1)
      |> Enum.join("\n")

    case String.length(response) do
      0 -> "No items"
      _ -> response
    end
  end

  def do_action({user, {:interactive_done, list_id}}) do
    ListBot.Lists.mark_item_as_done(user.id, list_id)
  end

  def do_action({user, {:interactive, _}}) do
    %{text: "Interactive menu", reply_markup: %{inline_keyboard: inline_keyboard(user.id)}}
  end

  def do_action({_, _}) do
    "Unknown command"
  end

  defp format_list_item({{_, text}, index}), do: "#{index + 1}. #{text}"

  defp do_done_action(_, index) when index < 0 do
    "Not a valid item id"
  end

  defp do_done_action(user_id, index) do
    item =
      ListBot.Lists.get_pending_list_items(user_id)
      |> Enum.at(index)

    case item do
      nil -> "Not a valid item id"
      {id, _} -> mark_item_as_done(id)
    end
  end

  defp mark_item_as_done(item_id) do
    item = ListBot.Lists.get_list_item!(item_id)

    case ListBot.Lists.update_list_item(item, %{done_at: DateTime.utc_now()}) do
      {:ok, _} -> "Done"
      {:error, _} -> "Failed to mark done"
    end
  end

  defp create_payload(text, length, offset) do
    body =
      String.slice(text, (offset + length)..-1)
      |> String.trim()

    case String.slice(text, offset, length) do
      "/list" ->
        {:list, body}

      "/add" ->
        {:add, body}

      "/done" ->
        {:done, body}

      "/interactive" ->
        {:interactive, body}

      _ ->
        @bad_command
    end
  end

  defp inline_keyboard_button({pk, list_item}) do
    [%{text: list_item, callback_data: "#{pk}"}]
  end

  defp inline_keyboard(user_id) do
    ListBot.Lists.get_pending_list_items(user_id)
    |> Enum.map(&inline_keyboard_button/1)
  end
end
