defmodule ListBot.Repo do
  use Ecto.Repo,
    otp_app: :list_bot,
    adapter: Ecto.Adapters.SQLite3
end
