defmodule ListBot.TelegramApi do
  use HTTPoison.Base

  @url "https://api.telegram.org/bot"

  def process_url(url) do
    @url <> Application.get_env(:list_bot, __MODULE__)[:api_key] <> url
  end

  def process_request_headers(headers) do
    [{"Content-Type", "application/json"} | headers]
  end

  def process_request_body(body) when is_map(body) do
    body |> Jason.encode!()
  end

  def set_webhook(url) do
    post("/setWebHook", %{url: url})
  end

  def clear_webhook, do: set_webhook("")
end
