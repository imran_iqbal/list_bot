defmodule ListBot.Telegram do
  use GenServer
  require Logger

  alias ListBot.TelegramApi

  import ListBot.Telegram.Actions, only: [get_command_payload: 2, do_action: 1]

  def start_link(initial_value) do
    GenServer.start_link(__MODULE__, initial_value, name: __MODULE__)
  end

  # Client State
  def handle_message(message), do: GenServer.cast(__MODULE__, {:handle_message, message})

  def handle_callback_query(callback_query),
    do: GenServer.cast(__MODULE__, {:handle_callback, callback_query})

  # Server State
  def init(init_arg) do
    {:ok, init_arg}
  end

  def handle_cast({:handle_callback, callback}, state) do
    Logger.info("Called with callback: #{inspect(callback)}")

    %{
      "id" => callback_id,
      "data" => callback_data,
      "message" => %{"message_id" => message_id, "chat" => %{"id" => chat_id}}
    } = callback

    send_message_helper(%{callback_query_id: callback_id}, "/answerCallbackQuery")

    user = ListBot.Accounts.get_user_from_message(callback)

    case user do
      nil ->
        nil

      _ ->
        process_callback(callback_data, message_id, chat_id, user)
    end

    {:noreply, state}
  end

  def handle_cast({:handle_message, message}, state) do
    Logger.info("Called with message: #{inspect(message)}")

    %{"chat" => %{"id" => chat_id}} = message
    user = ListBot.Accounts.get_user_from_message(message)

    case user do
      nil ->
        send_unauthed_message(chat_id)

      _ ->
        Logger.debug("Found user #{inspect(user)}")
        process_message(message, user, chat_id)
    end

    {:noreply, state}
  end

  defp send_unauthed_message(chat_id) do
    send_sticker("CAACAgIAAxkBAAM1YIY03o6aTA4sYk1Yqhu8nEYcKrgAAmQAA8GcYAzN3q9FnoaxIR8E", chat_id)
  end

  defp process_callback(callback_data, message_id, chat_id, user) do
    do_action({user, {:interactive_done, callback_data}})

    do_action({user, {:interactive, ""}})
    |> edit_message(message_id, chat_id)
  end

  defp process_message(%{"text" => text, "entities" => entities}, user, chat_id) do
    get_command_payload(text, entities)
    |> process_message(user, chat_id)
  end

  defp process_message(payload = {_, _}, user, chat_id) do
    response =
      {user, payload}
      |> do_action

    response =
      case payload do
        {command, _} when command in [:add, :done] ->
          response <> "\n\n" <> do_action({user, {:list, nil}})

        _ ->
          response
      end

    send_message(response, chat_id)
  end

  defp process_message(_, _user, chat_id) do
    send_message("Unknown command, type / to list commands", chat_id)
  end

  defp send_message_helper(message, chat_id, :sticker) do
    %{chat_id: chat_id, sticker: message}
    |> send_message_helper("/sendSticker")
  end

  defp send_message_helper(message, chat_id, :message) when is_binary(message) do
    %{chat_id: chat_id, text: message}
    |> send_message_helper("/sendMessage")
  end

  defp send_message_helper(message, chat_id, :message) when is_map(message) do
    message
    |> Map.put(:chat_id, chat_id)
    |> send_message_helper("/sendMessage")
  end

  defp edit_message_helper(message, message_id, chat_id, :edit_message) do
    message
    |> Map.merge(%{message_id: message_id, chat_id: chat_id})
    |> send_message_helper("/editMessageText")
  end

  defp send_message_helper(body, method) do
    Logger.info("Sending message #{inspect(body)}")

    response = TelegramApi.post(method, body)

    case response do
      {:ok, response} ->
        Logger.info("Success #{response.body}")

      {:error, reason} ->
        Logger.error("Failure #{reason}")
    end
  end

  defp send_sticker(sticker, chat_id) do
    send_message_helper(sticker, chat_id, :sticker)
  end

  defp send_message(message, chat_id) do
    send_message_helper(message, chat_id, :message)
  end

  defp edit_message(message, message_id, chat_id) do
    edit_message_helper(message, message_id, chat_id, :edit_message)
  end
end
