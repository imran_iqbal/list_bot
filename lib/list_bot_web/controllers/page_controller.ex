defmodule ListBotWeb.PageController do
  use ListBotWeb, :controller
  require Logger

  def index(conn, _params) do
    render(conn, "index.html")
  end

  def update(conn, %{"message" => message}) do
    ListBot.Telegram.handle_message(message)
    conn |> json(%{handled: true})
  end

  def update(conn, %{"callback_query" => callback_query}) do
    ListBot.Telegram.handle_callback_query(callback_query)
    conn |> json(%{handled: true})
  end

  def update(conn, _params) do
    conn |> json(%{success: true})
  end
end
